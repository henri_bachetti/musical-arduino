
#include "Arduino.h"
#include <SD.h>

// Liste des notes
#define SIL                     0
#define DON                     65
#define DOD                     69
#define REN                     74
#define RED                     78
#define MIN                     83
#define FAN                     87
#define FAD                     93
#define SON                     98
#define SOD                     104
#define LAN                     110
#define LAD                     117
#define SIN                     123
#define FIN                     255

// liste des rythmes
#define QUINTUPLECROCHE         1
#define QUADRUPLECROCHE         2
#define TRIPLECROCHE            4
#define DOUBLECROCHE            8
#define CROCHE                  16
#define CROCHEPOINTEE           24
#define NOIRE                   32
#define NOIREPOINTEE            48
#define BLANCHE                 64
#define BLANCHEPOINTEE          96
#define RONDE                   128

// liste des rythmiques de silences
#define TRENTEDEUXIEMEDESOUPIR  1
#define SEIZIEMEDESOUPIR        2
#define HUITIEMEDESOUPIR        4
#define QUARDESOUPIR            8
#define DEMISOUPIR              16
#define SOUPIR                  32
#define DEMIPAUSE               64
#define PAUSE                   128

// Tempo
#define LENTO                   60
#define ANDANTE                 80
#define MODERATO                100
#define ALLEGRO                 120
#define PRESTO                  150
#define PRESTISSIMO             200

#define MAX_SIZE                201
#define BUTTON                  8
#define SPEAKER                 9

struct note
{
  byte base;
  byte tempo;
  byte octave;
};

class MusicPlayer
{
  protected:
    byte m_speakerPin; // numero de pin
    int m_tempo;
    byte m_track;

  public:
    MusicPlayer();
    MusicPlayer(byte speakerPin);
    bool openDir(char *name);
    bool closeDir(void);
    int count(void);
    bool play(char *name, int tempo);
    bool playNext(int tempo);
    File getNext(void);
    void playFileHandle(File f, int tempo);

  private:
    File m_musicDir;
    struct note m_melody[MAX_SIZE];
    int getFrequency(int frequency, int octave);
    bool readMelody(File &f);
};

MusicPlayer::MusicPlayer(byte speakerPin)
{
  m_speakerPin = speakerPin;
  pinMode(speakerPin, OUTPUT);
}

bool MusicPlayer::openDir(char *name)
{
  Serial.print(F("open "));
  Serial.println(name);
  m_musicDir = SD.open(name);
  return true;
}

bool MusicPlayer::closeDir(void)
{
  Serial.print(F("close "));
  Serial.println(m_musicDir.name());
  m_musicDir.close();
  return true;
}

int MusicPlayer::count(void)
{
  int count = 0;
  File f;

  m_musicDir.rewindDirectory();
  while((f = m_musicDir.openNextFile()) != 0) {
    Serial.println(f.name());
    count++;
    f.close();
  }
  m_musicDir.rewindDirectory();
  return count;
}

int MusicPlayer::getFrequency(int frequency, int octave) {
  return frequency * pow(2, octave);
}

bool MusicPlayer::readMelody(File &f)
{
  int i = 0;

  while (f.available() && i < MAX_SIZE) {
    m_melody[i].base = f.read();
    m_melody[i].tempo = f.read();
    m_melody[i++].octave = f.read();
  }
  Serial.print(i);
  Serial.print(F(" notes from "));
  Serial.println(f.name());
  m_melody[i].base = FIN;
  f.close();
  return true;
}

void MusicPlayer::playFileHandle(File f, int tempo)
{
  float ftempo = 0;
  ftempo = (1000.0 * 60.0) / float(tempo);

  for (int n = 0; m_melody[n].base != FIN; n++) { // boucle de lecture du tableau
    char note = m_melody[n].base; // on récupère la fréquence de base dans le tableau
    char octave = m_melody[n].octave; // on récupère l'octave
    int frequence;
    if (note != SIL) {
      frequence = getFrequency(note, octave); //on calcule la bonne fréquence
    }
    //    Serial.println(frequence);
    float duree = ftempo * m_melody[n].tempo / 32; // on multiplie la duree de base par la valeur de duree du tableau
    //    Serial.println(duree);
    if (note != SIL) {
      tone (m_speakerPin, frequence); //on joue la note
    }
    delay(duree);
    if (note != SIL) {
      noTone(m_speakerPin);//on arrete la note
    }
    delay(10);// petite attente pour avoir l'impression d'attaquer la note
    if (digitalRead(BUTTON) == LOW) {
      break;
    }
  }
}

File MusicPlayer::getNext(void)
{
  if (m_musicDir == 0) {
    Serial.println(F("must open a directory first"));
    return File();
  }
  File f =  m_musicDir.openNextFile();
  if (f == 0) {
    m_musicDir.rewindDirectory();
    f =  m_musicDir.openNextFile();
  }
  Serial.print(F("##### get next: "));
  Serial.println(f.name());
  readMelody(f);
  return f;
}

bool MusicPlayer::playNext(int tempo)
{
  float ftempo = 0;
  bool play = false;

  if (m_musicDir == 0) {
    Serial.println(F("must open a directory first"));
    return false;
  }
  File f =  m_musicDir.openNextFile();
  if (f == 0) {
    m_musicDir.rewindDirectory();
    f =  m_musicDir.openNextFile();
  }
  Serial.print(F("##### play next: "));
  Serial.println(f.name());
  play = readMelody(f);
  ftempo = (1000.0 * 60.0) / float(tempo);
  if (play) {
    playFileHandle(f, tempo);
  }
  f.close();
  return true;
}

bool MusicPlayer::play(char *name, int tempo)
{
  bool play = false;

  File f =  SD.open(name);
  if (f == 0) {
    Serial.println(F("could not open file"));
    return false;
  }
  Serial.print(F("##### play: "));
  Serial.println(f.name());
  play = readMelody(f);
  if (play) {
    playFileHandle(f, tempo);
  }
  f.close();
  return true;
}

MusicPlayer player = MusicPlayer(SPEAKER);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(BUTTON, INPUT_PULLUP);
  if (!SD.begin(10)) {
    Serial.println(F("Card failed, or not present"));
    // don't do anything more:
    while (1);
  }
  Serial.println(F("card initialized."));
}

struct playlist
{
  char *name;
  uint8_t tempo;
};

struct playlist playlist[] = {"FRENCH", PRESTO,
                              "MARCHE", MODERATO,
                              "BAROQU", PRESTO,
                              0};

uint8_t cycle = 0;

void loop()
{
  int count;

  Serial.print(F("loop: "));
  Serial.println(cycle);
  if (cycle) {
    if (cycle == 1) {
      player.openDir("/ZIC");
      count = player.count();
      Serial.print(count);
      Serial.println(" files");
    }
    File f = player.getNext();
    uint8_t tempo = ALLEGRO;
    for (int i = 0 ; playlist[i].name ; i++) {
      if (strncmp(f.name(), playlist[i].name, 6) == 0) {
        tempo = playlist[i].tempo;
        break;
      }
    }
    player.playFileHandle(f, tempo);
    if (cycle == 5) {
      player.closeDir();
      cycle = 0;
    }
    else {
      cycle++;
    }
  }
  else {
    player.play("/ZIC/FRENCH~1.ZIC", PRESTO);
    cycle++;
  }
  delay(1000);
}

