
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// Liste des notes
#define SIL 0
#define DON 65
#define DOD 69
#define REN 74
#define RED 78
#define MIN 83
#define FAN 87
#define FAD 93
#define SON 98
#define SOD 104
#define LAN 110
#define LAD 117
#define SIB 117
#define SIN 123
#define FIN 255

// liste des rythmes
#define QUINTUPLECROCHE 1
#define QUADRUPLECROCHE 2
#define TRIPLECROCHE 4
#define DOUBLECROCHE 8
#define CROCHE 16
#define CROCHEPOINTEE 24
#define NOIRE 32
#define NOIREPOINTEE 48
#define BLANCHE 64
#define BLANCHEPOINTEE 96
#define RONDE 128

// liste des rythmiques de silences
#define TRENTEDEUXIEMEDESOUPIR 1
#define SEIZIEMEDESOUPIR 2
#define HUITIEMEDESOUPIR 4
#define QUARDESOUPIR 8
#define DEMISOUPIR 16
#define SOUPIR 32
#define DEMIPAUSE 64
#define PAUSE 128

// Tempo
#define LENTO 60
#define ANDANTE 80
#define MODERATO 100
#define ALLEGRO 120
#define PRESTO 150
#define PRESTISSIMO 200

struct note
{
  uint8_t base;
  uint8_t tempo;
  uint8_t oct;
};

// Mélodies
const struct note SON_TEST[] = {
  {DON, CROCHE, 0},
  {DON, CROCHE, 2},
  {DON, NOIRE, 3},
  FIN
};

const struct note french_cancan[] = {
  {SON, NOIRE, 3}, {DON, BLANCHE, 3}, {REN, CROCHE, 3}, {FAN, CROCHE, 3},
  {MIN, CROCHE, 3}, {REN, CROCHE, 3}, {SON, NOIRE, 3}, {SON, NOIRE, 3},
  {SON, CROCHE, 3}, {LAN, CROCHE, 3}, {MIN, CROCHE, 3}, {FAN, CROCHE, 3},
  {REN, NOIRE, 3}, {REN, NOIRE, 3}, {REN, CROCHE, 3}, {FAN, CROCHE, 3},
  {MIN, CROCHE, 3}, {REN, CROCHE, 3}, {DON, CROCHE, 3}, {DON, CROCHE, 4},
  {SIN, CROCHE, 3}, {LAN, CROCHE, 3}, {SON, CROCHE, 3}, {FAN, CROCHE, 3},
  {MIN, CROCHE, 3}, {REN, CROCHE, 3}, {DON, BLANCHE, 3}, {REN, CROCHE, 3},
  {FAN, CROCHE, 3}, {MIN, CROCHE, 3}, {REN, CROCHE, 3}, {SON, NOIRE, 3},
  {SON, NOIRE, 3}, {SON, CROCHE, 3}, {LAN, CROCHE, 3}, {MIN, CROCHE, 3},
  {FAN, CROCHE, 3}, {REN, NOIRE, 3}, {REN, NOIRE, 3}, {REN, CROCHE, 3},
  {SON, CROCHE, 3}, {REN, CROCHE, 3}, {MIN, CROCHE, 3}, {DON, NOIRE, 3},
  FIN
};

const struct note marche_imperiale[] = {
  {SON, NOIRE, 3}, {SON, NOIRE, 3}, {SON, CROCHE, 3}, {RED, CROCHEPOINTEE, 3},
  {LAD, DOUBLECROCHE, 3}, {SON, NOIRE, 3}, {RED, CROCHEPOINTEE, 3}, {LAD, DOUBLECROCHE, 3},
  {SON, BLANCHE, 3}, {REN, NOIRE, 4}, {REN, NOIRE, 4}, {REN, NOIRE, 4},
  {RED, CROCHEPOINTEE, 4}, {LAD, DOUBLECROCHE, 3}, {FAD, NOIRE, 3}, {RED, CROCHEPOINTEE, 3},
  {LAD, DOUBLECROCHE, 3}, {SON, BLANCHE, 3}, {SON, NOIRE, 4}, {SON, CROCHEPOINTEE, 3},
  {SON, DOUBLECROCHE, 3}, {SON, NOIRE, 4}, {FAD, CROCHEPOINTEE, 4}, {FAN, DOUBLECROCHE, 4},
  {MIN, DOUBLECROCHE, 4}, {RED, DOUBLECROCHE, 4}, {MIN, CROCHE, 4}, {SIL, DEMISOUPIR, 0},
  {SON, CROCHE, 3}, {REN, NOIRE, 4}, {DON, CROCHEPOINTEE, 4}, {SIN, DOUBLECROCHE, 3},
  {LAD, DOUBLECROCHE, 3}, {LAN, DOUBLECROCHE, 3}, {LAD, CROCHE, 3}, {SIL, DEMISOUPIR, 0},
  {RED, CROCHE, 3}, {SON, NOIRE, 3}, {RED, CROCHEPOINTEE, 3}, {LAD, DOUBLECROCHE, 3},
  {SON, NOIRE, 3}, {RED, CROCHEPOINTEE, 3}, {LAD, DOUBLECROCHE, 3}, {SON, BLANCHE, 3},
  FIN
};

const struct note royal_fireworks[] = {
  {REN, NOIRE, 3}, {SON, CROCHE, 3}, {SON, CROCHE, 3}, {SON, CROCHE, 3},
  {SON, CROCHE, 3}, {SON, DOUBLECROCHE, 3}, {LAN, DOUBLECROCHE, 3}, {SIN, DOUBLECROCHE, 3},
  {LAN, DOUBLECROCHE, 3}, {SON, CROCHE, 3}, {SON, DOUBLECROCHE, 3}, {LAN, DOUBLECROCHE, 3},
  {SIN, CROCHE, 3}, {SIN, CROCHE, 3}, {SIN, CROCHE, 3}, {SIN, CROCHE, 3},
  {SIN, DOUBLECROCHE, 3}, {DON, DOUBLECROCHE, 4}, {REN, DOUBLECROCHE, 4}, {DON, DOUBLECROCHE, 4},
  {SIN, CROCHE, 3}, {SIN, DOUBLECROCHE, 3}, {DON, DOUBLECROCHE, 4}, {REN, CROCHE, 4},
  {REN, CROCHE, 4}, {REN, CROCHE, 4}, {REN, CROCHE, 4}, {REN, BLANCHE, 4},
  {SIL, DEMISOUPIR, 0}, {SON, CROCHE, 4}, {REN, CROCHE, 4}, {SIN, CROCHE, 3},
  {SIL, DEMISOUPIR, 0}, {SON, CROCHE, 4}, {REN, CROCHE, 4}, {SIN, CROCHE, 3},
  {SIL, DEMISOUPIR, 0}, {REN, NOIRE, 4}, {REN, NOIRE, 4}, {MIN, DOUBLECROCHE, 4},
  {REN, DOUBLECROCHE, 4}, {DON, CROCHE, 4}, {SIN, CROCHE, 3}, {LAN, BLANCHE, 3},
  {SIL, DEMISOUPIR, 0}, {REN, CROCHE, 4}, {REN, CROCHE, 4}, {MIN, DOUBLECROCHE, 4},
  {REN, DOUBLECROCHE, 4}, {DON, CROCHE, 4}, {SIN, CROCHE, 3}, {LAN, CROCHEPOINTEE, 3},
  {SIN, DOUBLECROCHE, 3}, {LAN, CROCHEPOINTEE, 3}, {SIN, DOUBLECROCHE, 3}, {LAN, DOUBLECROCHE, 3},
  {SIN, DOUBLECROCHE, 3}, {LAN, DOUBLECROCHE, 3}, {SIN, DOUBLECROCHE, 3}, {LAN, DOUBLECROCHE, 3},
  {SIN, DOUBLECROCHE, 3}, {LAN, DOUBLECROCHE, 3}, {SIN, DOUBLECROCHE, 3}, {LAN, CROCHEPOINTEE, 3},
  {LAN, DOUBLECROCHE, 3}, {SIN, DOUBLECROCHE, 3}, {DON, DOUBLECROCHE, 4}, {REN, DOUBLECROCHE, 4},
  {MIN, DOUBLECROCHE, 4}, {DOD, CROCHEPOINTEE, 4}, {REN, DOUBLECROCHE, 4}, {REN, BLANCHE, 4},
  FIN
};

const struct note marseillaise[] = {
  {REN, DOUBLECROCHE, 3}, {REN, CROCHEPOINTEE, 3}, {REN, DOUBLECROCHE, 3}, {SON, NOIRE, 3}, {SON, NOIRE, 3},
  {LAN, NOIRE, 3},  {LAN, NOIRE, 3}, {REN, NOIREPOINTEE, 4}, {SIN, CROCHE, 3}, {SON, CROCHEPOINTEE, 3},
  {SON, DOUBLECROCHE, 3}, {SIN, CROCHEPOINTEE, 3}, {SON, DOUBLECROCHE, 3},

  {MIN, NOIRE, 3}, {DON, BLANCHE, 4}, {LAN, CROCHEPOINTEE, 3}, {FAD, DOUBLECROCHE, 3}, {SON, BLANCHE, 3}, {SIL, SOUPIR, 3},
  {SON, CROCHEPOINTEE, 3}, {LAN, DOUBLECROCHE, 3}, {SIN, NOIRE, 3}, {SIN, NOIRE, 3},   {SIN, NOIRE, 3},
  {DON, CROCHEPOINTEE, 4}, {SIN, DOUBLECROCHE, 3},

  {SIN, NOIRE, 3}, {LAN, NOIRE, 3}, {SIL, SOUPIR, 3}, {LAN, CROCHEPOINTEE, 3}, {SIN, DOUBLECROCHE, 3}, {DON, NOIRE, 4},
  {DON, NOIRE, 4}, {DON, NOIRE, 4}, {REN, CROCHEPOINTEE, 4}, {DON, DOUBLECROCHE, 4}, {SIN, BLANCHE, 3}, {SIL, SOUPIR, 3},
  {REN, CROCHEPOINTEE, 4}, {REN, DOUBLECROCHE, 4},

  {REN, NOIRE, 4}, {SIN, CROCHEPOINTEE, 3}, {SON, DOUBLECROCHE, 3}, {REN, NOIRE, 4}, {SIN, CROCHEPOINTEE, 3},
  {SON, DOUBLECROCHE, 3}, {REN, BLANCHE, 3}, {SIL, DEMISOUPIR, 3}, {REN, DOUBLECROCHE, 3},
  {REN, CROCHEPOINTEE, 3}, {FAD, DOUBLECROCHE, 3}, {LAN, BLANCHE, 3}, {DON, NOIRE, 4}, {LAN, CROCHEPOINTEE, 3},
  {FAD, DOUBLECROCHE, 3},

  {LAN, NOIRE, 3}, {SON, NOIRE, 3}, {FAN, BLANCHE, 3}, {MIN, NOIRE, 3}, {SON, CROCHEPOINTEE, 3}, {SON, DOUBLECROCHE, 3},
  {SON, NOIRE, 3}, {FAD, CROCHEPOINTEE, 3}, {SON, DOUBLECROCHE, 3}, {LAN, BLANCHEPOINTEE, 3}, {SIL, DEMISOUPIR, 3}, {LAN, CROCHE, 3},
  {SIB, NOIREPOINTEE, 3}, {SIB, CROCHE, 3}, {SIB, CROCHE, 3}, {SIB, CROCHE, 3}, {DON, CROCHE, 4}, {REN, CROCHE, 4},

  {LAN, BLANCHEPOINTEE, 3}, {SIB, CROCHE, 3}, {LAN, CROCHE, 3}, {SON, NOIREPOINTEE, 3}, {SON, CROCHE, 3}, {SON, CROCHE, 3},
  {SIB, CROCHE, 3}, {LAN, CROCHE, 3}, {SON, CROCHE, 3}, {SON, NOIRE, 3}, {FAD, CROCHE, 3}, {SIL, DEMIPAUSE, 3},
  {REN, DOUBLECROCHE, 4},

  {REN, BLANCHE+CROCHEPOINTEE, 4}, {REN, DOUBLECROCHE, 4}, {SIN, CROCHEPOINTEE, 3},
  {SON, DOUBLECROCHE, 3}, {LAN, BLANCHEPOINTEE, 3}, {SIL, DEMISOUPIR, 4}, {REN, DOUBLECROCHE, 4},
  {REN, BLANCHE+CROCHEPOINTEE, 4}, {REN, DOUBLECROCHE, 4}, {SIN, CROCHEPOINTEE, 3}, {SON, DOUBLECROCHE, 3}, {LAN, BLANCHEPOINTEE, 3},
  {REN, NOIRE, 3}, {SON, BLANCHEPOINTEE, 3}, {LAN, NOIRE, 3},

  {SIN, RONDE, 3}, {DON, BLANCHE, 4}, {REN, NOIRE, 4}, {MIN, NOIRE, 4}, {LAN, BLANCHEPOINTEE, 3}, {MIN, NOIRE, 4}, {REN, BLANCHE, 4},
  {REN, CROCHEPOINTEE, 4}, {SIN, DOUBLECROCHE, 3}, {DON, CROCHEPOINTEE, 4}, {LAN, DOUBLECROCHE, 3}, {SON, RONDE, 3},

  FIN
};

const struct note baroque[] = {
  {DON, CROCHE, 4}, {SIN, CROCHE, 3}, {LAN, CROCHE, 3}, {FAN, CROCHE, 3}, {FAN, CROCHE, 3}, {REN, CROCHE, 3},
  {FAN, CROCHE, 3}, {FAN, CROCHE, 3}, {LAN, CROCHE, 3}, {FAN, CROCHE, 3}, {LAN, CROCHE, 3}, {DON, CROCHE, 4},
  {SIN, CROCHE, 3}, {LAN, CROCHE, 3},
  {SIN, CROCHE, 3}, {SON, CROCHE, 3}, {SON, CROCHE, 3}, {REN, CROCHE, 3},
  {SON, CROCHE, 3}, {SON, CROCHE, 3}, {SIN, CROCHE, 3}, {SON, CROCHE, 3}, {SIN, CROCHE, 3}, {REN, CROCHE, 3},
  {DON, CROCHE, 4}, {SIN, CROCHE, 3},
  {LAN, CROCHE, 3}, {FAN, CROCHE, 3}, {FAN, CROCHE, 3}, {REN, CROCHE, 3}, {FAN, CROCHE, 3}, {FAN, CROCHE, 3},
  {LAN, CROCHE, 3}, {FAN, CROCHE, 3}, {LAN, CROCHE, 3}, {DON, CROCHE, 4}, {SIN, CROCHE, 3}, {LAN, CROCHE, 3},
  {SIN, CROCHE, 3}, {LAN, CROCHE, 3}, {SIN, CROCHE, 3}, {SON, CROCHE, 3}, {DON, CROCHE, 4}, {SIN, CROCHE, 3},
  {LAN, CROCHE, 3}, {FAN, CROCHE, 3}, {FAN, CROCHE, 3}, {FAN, CROCHE, 3},
  FIN
};

int main(int argc, char **argv)
{
	char *fname;
	int i;

	fname = "french-cancan.zic";
	FILE *fd = fopen(fname, "w");
	if (fd == 0) {
		perror(fname);
		exit(1);
	}
	for (i = 0 ; french_cancan[i].base != FIN ; i++) {
		if (fwrite(&french_cancan[i], sizeof(struct note), 1, fd) != 1) {
			perror(fname);
			fclose(fd);
			exit(1);
		}
	}
	fclose(fd);
	printf("%s: %d notes\n", fname, i);

	fname = "marche-imperiale.zic";
	fd = fopen(fname, "w");
	if (fd == 0) {
		perror(fname);
		exit(1);
	}
	for (i = 0 ; marche_imperiale[i].base != FIN ; i++) {
		if (fwrite(&marche_imperiale[i], sizeof(struct note), 1, fd) != 1) {
			perror(fname);
			fclose(fd);
			exit(1);
		}
	}
	fclose(fd);
	printf("%s: %d notes\n", fname, i);

	fname = "fireworks.zic";
	fd = fopen(fname, "w");
	if (fd == 0) {
		perror(fname);
		exit(1);
	}
	for (i = 0 ; royal_fireworks[i].base != FIN ; i++) {
		if (fwrite(&royal_fireworks[i], sizeof(struct note), 1, fd) != 1) {
			perror(fname);
			fclose(fd);
			exit(1);
		}
	}
	fclose(fd);
	printf("%s: %d notes\n", fname, i);

	fname = "marseillaise.zic";
	fd = fopen(fname, "w");
	if (fd == 0) {
		perror(fname);
		exit(1);
	}
	for (i = 0 ; marseillaise[i].base != FIN ; i++) {
		if (fwrite(&marseillaise[i], sizeof(struct note), 1, fd) != 1) {
			perror(fname);
			fclose(fd);
			exit(1);
		}
	}
	fclose(fd);
	printf("%s: %d notes\n", fname, i);
	fname = "baroque.zic";
	fd = fopen(fname, "w");
	if (fd == 0) {
		perror(fname);
		exit(1);
	}
	for (i = 0 ; baroque[i].base != FIN ; i++) {
		if (fwrite(&baroque[i], sizeof(struct note), 1, fd) != 1) {
			perror(fname);
			fclose(fd);
			exit(1);
		}
	}
	fclose(fd);
	printf("%s: %d notes\n", fname, i);
	return 0;
}

