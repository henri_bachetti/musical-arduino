# MUSICAL ARDUINO

The purpose of this page is to explain step by step the realization of a music player based on ARDUINO NANO, connected to a SDCARD module.

The board uses the following components :

 * an ARDUINO NANO
 * a SDCARD module
 * a push-button
 * the board is powered by the USB.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2018/11/un-arduino-musical.html